loading cache ./config.cache
checking for gcc... (cached) gcc
checking whether the C compiler (gcc  ) works... yes
checking whether the C compiler (gcc  ) is a cross-compiler... no
checking whether we are using GNU C... (cached) yes
checking whether gcc accepts -g... (cached) yes
checking host system type... i686-pc-linux-gnu
checking build system type... i686-pc-linux-gnu
checking for ld used by GCC... (cached) /usr/bin/ld
checking if the linker (/usr/bin/ld) is GNU ld... (cached) yes
checking for a BSD compatible install... (cached) /usr/bin/install -c
checking whether make sets ${MAKE}... (cached) yes
checking how to run the C preprocessor... (cached) gcc -E
checking whether gcc needs -traditional... (cached) no
checking for AIX... no
checking for minix/config.h... (cached) no
checking for POSIXized ISC... no
checking for gcc option to accept ANSI C... (cached) none needed
checking for ANSI C header files... (cached) yes
checking for fcntl.h... (cached) yes
checking for unistd.h... (cached) yes
checking for libintl.h... (cached) yes
checking for libc.h... (cached) no
checking for sys/dsreq.h... (cached) no
checking for sys/select.h... (cached) yes
checking for sys/time.h... (cached) yes
checking for jconfig.h... (cached) yes
checking for sys/scanio.h... (cached) no
checking for scsi.h... (cached) no
checking for sys/scsi.h... (cached) no
checking for sys/scsicmd.h... (cached) no
checking for sys/scsiio.h... (cached) no
checking for bsd/dev/scsireg.h... (cached) no
checking for scsi/sg.h... (cached) yes
checking for /usr/src/linux/include/scsi/sg.h... (cached) no
checking for io/cam/cam.h... (cached) no
checking for camlib.h... (cached) no
checking for os2.h... (cached) no
checking for sys/socket.h... (cached) no
checking for sys/io.h... (cached) no
checking for asm/io.h... (cached) no
checking for gscdds.h... (cached) no
checking for sys/hw.h... (cached) no
checking for sys/types.h... (cached) yes
checking for sys/scsi/sgdefs.h... (cached) no
checking for sys/scsi/targets/scgio.h... (cached) no
checking for apollo/scsi.h... (cached) no
checking for sys/sdi_comm.h... (cached) no
checking for sys/passthrudef.h... (cached) no
checking for working const... (cached) yes
checking for inline... (cached) inline
checking return type of signal handlers... (cached) int
checking for size_t... (cached) yes
checking for pid_t... (cached) yes
checking for ssize_t... (cached) yes
checking for u_char... (cached) yes
checking for u_int... (cached) yes
checking for u_long... (cached) yes
checking for scsireq_enter in -lscsi... (cached) no
checking for cam_open_device in -lcam... (cached) no
checking for sqrt in -lm... (cached) yes
checking for gettext in -lintl... (cached) no
checking for syslog in -lsyslog... (cached) no
checking for jpeg_start_decompress in -ljpeg... (cached) yes
checking for gethostbyaddr in -lnsl... (cached) yes
checking for socket in -lsocket... (cached) no
checking for working alloca.h... (cached) yes
checking for alloca... (cached) yes
checking for unistd.h... (cached) yes
checking for getpagesize... (cached) yes
checking for working mmap... (cached) yes
checking for atexit... (cached) yes
checking for ioperm... (cached) yes
checking for mkdir... (cached) yes
checking for scsireq_enter... (cached) no
checking for sigprocmask... (cached) yes
checking for strdup... (cached) yes
checking for strndup... (cached) yes
checking for strftime... (cached) yes
checking for strstr... (cached) yes
checking for strsep... (cached) yes
checking for strtod... (cached) yes
checking for snprintf... (cached) yes
checking for cfmakeraw... (cached) yes
checking for tcsendbreak... (cached) yes
checking for usleep... (cached) yes
checking for strcasecmp... (cached) yes
checking for strncasecmp... (cached) yes
checking for _portaccess... (cached) no
checking for ranlib... (cached) ranlib
checking for BSD-compatible nm... (cached) /usr/bin/nm -B
checking whether ln -s works... (cached) yes
loading cache ./config.cache within ltconfig
checking for object suffix... o
checking for executable suffix... (cached) no
checking for gcc option to produce PIC... -fPIC
checking if gcc PIC flag -fPIC works... yes
checking if gcc supports -c -o file.o... yes
checking if gcc supports -c -o file.lo... yes
checking if gcc supports -fno-rtti -fno-exceptions ... yes
checking if gcc static flag -static works... -static
checking if the linker (/usr/bin/ld) is GNU ld... yes
checking whether the linker (/usr/bin/ld) supports shared libraries... yes
checking command to parse /usr/bin/nm -B output... ok
checking how to hardcode library paths into programs... immediate
checking for /usr/bin/ld option to reload object files... -r
checking dynamic linker characteristics... Linux ld.so
checking if libtool supports shared libraries... yes
checking whether to build shared libraries... yes
checking whether to build static libraries... yes
checking for objdir... .libs
creating libtool
loading cache ./config.cache
checking for X... (cached) libraries /usr/X11R6/lib, headers /usr/X11R6/include
checking for dlfcn.h... (cached) yes
checking for dlopen in -ldl... (cached) yes
checking for dlopen... (cached) yes
checking for dl.h... (cached) no
checking for gtk-config... (cached) /usr/bin/gtk-config
checking for GTK - version >= 0.99.13... no
*** Could not run GTK test program, checking why...
*** The test program failed to compile or link. See the file config.log for the
*** exact error that occured. This usually means GTK was incorrectly installed
*** or that you have moved GTK since it was installed. In the latter case, you
*** may want to edit the gtk-config script: /usr/bin/gtk-config
enabling DC210 backend
disabling PINT backend
enabling QuickCam backend
disabling NET backend
scsi buffersize: 131072
creating ./config.status
creating Makefile
creating lib/Makefile
creating sanei/Makefile
creating frontend/Makefile
creating japi/Makefile
creating backend/Makefile
creating include/Makefile
creating doc/Makefile
creating tools/Makefile
creating tools/sane-config
creating include/sane/config.h
include/sane/config.h is unchanged
****************************************************************
* Please be sure to read file PROBLEMS in this directory       *
* BEFORE running any of the SANE applications.  Some devices   *
* may be damaged by inproper operation, so please do heed this *
* advice.                                                      *
****************************************************************
